# States

## Description

Simple Finite State Machine library.

State machines are a simple way to express the flow of a program through states and transitions, those, in turn, are defined in terms of easy rules, which typically include "where you can go to" and "is it ok to terminate the FSM here?". States currently only supports the first of those two, but may support the other one eventually.

GraphViz rendering may be supported eventually as well.

### Author

_Bowuigi_

### Repository

This egg is hosted on Codeberg:

https://codeberg.org/Bowuigi/States

### Requirements

**advice**

### Documentation

    [syntax] (state-machine INITIAL (TRIGGER ! STATE -> POSSIBLE-TRANSITIONS ...) ...)

Creates a new state machine whose initial state is `INITIAL`, each `TRIGGER` is a procedure that will be injected the state change (set the current state to `STATE`) / validation (check if `STATE` is in previous state's `POSSIBLE-TRANSITIONS`), thus working like a transition callback. `INITIAL`, `STATE` and `POSSIBLE-TRANSITIONS` must be symbols, and there must be a `POSSIBLE-TRANSITIONS` definition for `INITIAL` so transitioning is possible.

Transitions (via triggers) should be done in tail call position as to not overflow the stack and also to avoid incorrect-state bugs.

    [procedure] (machine-state MACHINE)

Retrieves the current state of `MACHINE`, mostly useful when a trigger is used for multiple states.

### Example

Simple Cyclic state machine, included inside the test suite:

    (import states)

    (define (a) (print "In state A"))
    (define (b) (print "In state B"))
    (define (c) (print "In state C"))
    
    (state-machine
      A
      (a ! A -> B)
      (b ! B -> C)
      (c ! C -> A))

	(b) ; A -> B
	(c) ; B -> C
	(a) ; C -> A

### License

Public Domain.

The code was written by Bowuigi and placed in the Public Domain. All warranties are disclaimed.

