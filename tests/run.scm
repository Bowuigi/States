(import test states)

;; Test 1, cyclic state machine
(let ((a (lambda () 'A))
      (b (lambda () 'B))
      (c (lambda () 'C)))
  (state-machine
    A
    (a ! A -> B)
    (b ! B -> C)
    (c ! C -> A))

  (test-group "Cyclic"
    ;; We are already on state A, so (a) is invalid here (A -> A)
    (test-error "A -> A (should fail)" (a))
    ;; Transitions that should work
    (test "A -> B (should work)" 'B (b))
    (test "B -> C (should work)" 'C (c))
    (test "C -> A (should work)" 'A (a))))

;; Test 2, simple string parsing
;; Pattern is /(a*b?)*/
(let* ((a (lambda () 'A))
       (b (lambda () 'B))
       (parse-list
         (lambda (l)
           (foldl
             (lambda (lst current)
               (append lst
                       (list
                         (case current
                           ((#\a) (a))
                           ((#\b) (b))
                           (else 'Error))))) '() l))))

  (state-machine
    A
    (a ! A -> A B)
    (b ! B -> A))

  (test-group "Parse /(a*b?)*/"
    (test-error "abb (should fail)" (parse-list (string->list "abb")))
    (test "aabaab (should work)" '(A A B A A B) (parse-list (string->list "aabaab")))
    (test "Empty string (should work)" '() (parse-list (string->list "")))))

(test-exit)

