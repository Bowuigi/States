(module states (state-machine machine-state %make-machine %make-transition)
  (import scheme
          (chicken base)
          (chicken syntax)
          advice)

;; initial-state is a symbol naming the initial state.
;; states is a list of the form
;; (state possible-transitions ...)
;; The function acts as a callback and as a trigger
;; Calling it transitions to that state
;; You should make transitions on tail-call position
(define-record machine states state)

(define %make-machine make-machine)

;; Try to transition to state, if not in the possible transitions, signal an error.
;; Low level API only available for the state machine macro
(define (%make-transition sm state)
  (let ((state-list (assoc (machine-state sm) (machine-states sm))))
    (unless (memq state (cdr state-list))
      (error "Forbidden transition"
             (string-append (symbol->string (machine-state sm))
                            " -> "
                            (symbol->string state))))

    (machine-state-set! sm state)))

(define-syntax state-machine
  (syntax-rules (! ->)
    ((_ initial-state
        (func ! state -> possible-transitions ...) ...)
     (let ((sm (%make-machine '((state possible-transitions ...) ...) 'initial-state))) 
       (advise 'before func (lambda args (%make-transition sm 'state))) ...
       sm))))

) ; Module end

